/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.hostel.register.ui.model.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class StudentDetails {
    private String name;
    private Integer id;
    private String department;
    private Double attendance;

    public StudentDetails(String name, Integer id, String department, Double attendance) {
        this.name = name;
        this.id = id;
        this.department = department;
        this.attendance = attendance;
    }
    private static final Logger LOG = Logger.getLogger(StudentDetails.class.getName());

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Double getAttendance() {
        return attendance;
    }

    public void setAttendance(Double attendance) {
        this.attendance = attendance;
    }
    
}
